var express = require('express');
var app = express();
var multer = require('multer');
var bodyParser = require('body-parser');
var fs = require('fs');
var urlencodedParser = bodyParser.urlencoded({extended:true});


app.set("view engine","ejs");

app.use(express.static('./public'));


  app.use(bodyParser.json())

var storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,"./public/temp");
    },
    filename:function(req,file,cb){
        cb(null,file.fieldname+"_"+Date.now()+".jpg");
    }
})


var upload = multer({storage:storage, fileFilter:function(req,file,cb){
    if(file.mimetype=="image/jpg" || file.mimetype=="image/jpeg" || file.mimetype=="image/png")
    {
        cb(null,true);
    }
    else
    {
        cb(null,false);
        return cb(new Error("Invalid file extension"));
    }
}});

app.post("/",function(req,res){
    console.log(req.body.user)
    res.render("index",{message:"none"});
})

app.post("/up",urlencodedParser,upload.single('ul_img'),function(req,res,next){
    const file = req.file;
    if(!file)
    {
        const error = new Error('Please upload a file');
        error.httpStatusCode = 400;
        return next(error);
    }   
    else
    {
        console.log(file.filename);
        console.log(file.size);
        res.render("index",{message:"File uploaded successfully"});
    }
});


app.post("/upapi",urlencodedParser,function(req,res){
    let file = req.body.user.name;
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear().toString()  + (current_datetime.getMonth() + 1).toString()  +current_datetime.getDate().toString() + current_datetime.getHours().toString() + current_datetime.getMinutes().toString()  + current_datetime.getSeconds().toString(); 
    let des = file.lastIndexOf('\\');
    let ext = file.lastIndexOf('.');
    des = formatted_date +"_"+file.slice(des+1);
    console.log(des);
    if (file.slice(ext+1)=='jpg'||file.slice(ext+1)=='jpeg'||file.slice(ext+1)=='png')
    {
        fs.copyFile(file,'./public/temp/'+des,(err)=>{
            if (err)
                throw err;
            res.send("uploaded successfully");
       });
    }
    else{
        res.send("invalid file type");
    }
});

app.get("/chat",function(req,res){
    res.render('chat');
})

server = app.listen(4500);

const io = require('socket.io')(server);

io.on('connection',(socket) => {
    console.log("New user connected");  
    socket.username = "Anoymous";

    socket.on('change_username',(data)=>{
        socket.username = data.username;
    })

    socket.on('new_message',(data)=>{
        io.sockets.emit('new_message',{message:data.message,username:socket.username});
    })
})

