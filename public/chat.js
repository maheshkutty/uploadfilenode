$(function(){
    var socket = io.connect('http://localhost:4500');
    $("#send").on("click",function(){
        socket.emit('new_message',{message :  $('#msg').val()}); // above message emit to server 
    });
    socket.on('new_message',(data)=>{ // this method is listener which actually get message form the other client 
        console.log(data.username);
        var msg1 = document.createElement("li");
        var iData = $('#msg').val();
        if(!iData)
        {
            alert("Add some chat");
        }
        else
        {
            msg1.innerHTML = data.username+":"+data.message;  
            msg1.style.color = "white";
            msg1.style.alignContent = "bottom";
            msg1.style.float = "right";
            msg1.style.clear = "right";
            msg1.style.backgroundColor = "purple";
            msg1.style.fontSize = "20px";
            msg1.style.width = "200px";
            msg1.style.margin = "10px";
            msg1.style.borderRadius = "5px";
            msg1.style.position= "relative";
            $('#msgContainer').append(msg1);
        }
    });
    $("#userSub").on("click",function(){
        //var username = $('#username').val();
        console.log($('#username').val());
        socket.emit('change_username',{username : $('#username').val()});   // message to emit the user name so that we can know client name
     });
});
